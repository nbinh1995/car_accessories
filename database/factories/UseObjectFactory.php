<?php

namespace Database\Factories;

use App\Models\UseObject;
use Illuminate\Database\Eloquent\Factories\Factory;

class UseObjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UseObject::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
