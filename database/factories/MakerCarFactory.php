<?php

namespace Database\Factories;

use App\Models\MakerCar;
use Illuminate\Database\Eloquent\Factories\Factory;

class MakerCarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MakerCar::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
