<?php

namespace Database\Factories;

use App\Models\ModelCar;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ModelCarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ModelCar::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'Model_' . Str::random(15),
            'class_car_id' => rand(1, 25)
        ];
    }
}
