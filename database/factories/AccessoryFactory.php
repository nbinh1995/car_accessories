<?php

namespace Database\Factories;

use App\Models\Accessory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AccessoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Accessory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'Product_' . Str::random(15),
            'description' => $this->faker->paragraph(5),
            'price' => $this->faker->numberBetween(50000, 1500000),
            'brand' => 'Brand_' . Str::random(15),
            'manufacturing_date' => $this->faker->dateTimeBetween($startDate = '-30days', $endDate = '+30 days'),
            'category_id' => rand(1, 5)
        ];
    }
}
