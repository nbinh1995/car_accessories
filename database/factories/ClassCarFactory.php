<?php

namespace Database\Factories;

use App\Models\ClassCar;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClassCarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ClassCar::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
