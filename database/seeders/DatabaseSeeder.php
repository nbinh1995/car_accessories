<?php

namespace Database\Seeders;

use App\Models\Accessory;
use App\Models\Category;
use App\Models\ClassCar;
use App\Models\MakerCar;
use App\Models\ModelCar;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    protected $categories = ['Tire', 'Brake', 'Seat Covers', 'Sun Shades', 'Car Cover'];
    protected $makers = ['Toyota', 'Hyundai', 'Mazda', 'Ford', 'Honda'];
    protected $classes = ['Mini', 'Economy', 'Compact', 'Standard', 'Luxury'];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'ntqb',
            'email' => 'ntqb@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('123456789'), // password
            'is_admin' => 1,
            'remember_token' => Str::random(10),
        ]);
        foreach ($this->categories as $item) {
            Category::create(['name' => $item]);
        }

        foreach ($this->makers as $item) {
            MakerCar::create(['name' => $item]);
        }

        foreach ($this->classes as $item) {
            ClassCar::create(['name' => $item, 'maker_car_id' => 1]);
        }
        foreach ($this->classes as $item) {
            ClassCar::create(['name' => $item, 'maker_car_id' => 2]);
        }
        foreach ($this->classes as $item) {
            ClassCar::create(['name' => $item, 'maker_car_id' => 3]);
        }
        foreach ($this->classes as $item) {
            ClassCar::create(['name' => $item, 'maker_car_id' => 4]);
        }
        foreach ($this->classes as $item) {
            ClassCar::create(['name' => $item, 'maker_car_id' => 5]);
        }

        ModelCar::factory(20)->create();
        Accessory::factory(30)->create();
        // \App\Models\User::factory(10)->create();
    }
}
