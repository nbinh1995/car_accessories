<form action="{{$url}}" method="post" id="{{$type}}" enctype="multipart/form-data">
    @csrf
    @isset($item)
    @method('PATCH')
    <input type="hidden" name="id" value="{{$item->id}}">
    @endisset
    <div class="form-group row">
        <label for="name" class="col-md-12 col-form-label">{{ __('Name Category') }}</label>
        <div class="col-md-12">
            <input id="name" type="text" class="form-control" name="name" required autofocus placeholder="Name Category"
                value="{{$item->name ?? ''}}">
        </div>
    </div>
</form>