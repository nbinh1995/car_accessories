<form action="/dashboard/useObject" method="post" id="form_addobject" enctype="multipart/form-data">
    @csrf
    <div class="form-group row">
        <label for="maker_car_id" class="col-md-12 col-form-label">Maker Car</label>
        <div class="col-md-12">
            <select name="maker_car_id" id="maker_car_id" class="form-control select2" autofocus>
                <option disabled selected>Select item</option>
                @foreach ($makers as $value)
                <option value="{{$value->id}}">{{__($value->name)}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="class_car_id" class="col-md-12 col-form-label">Class Car</label>
        <div class="col-md-12">
            <select name="class_car_id" id="class_car_id" class="form-control select2" autofocus>
                <option disabled selected>Select item</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="model_car_id" class="col-md-12 col-form-label">Model Car</label>
        <div class="col-md-12">
            <select name="model_car_id" id="model_car_id" class="form-control select2" autofocus>
                <option disabled selected>Select item</option>
            </select>
        </div>
    </div>
</form>