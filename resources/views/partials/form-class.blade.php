<form action="{{$url}}" method="post" id="{{$type}}" enctype="multipart/form-data">
    @csrf
    @isset($item)
    @method('PATCH')
    <input type="hidden" name="id" value="{{$item->id}}">
    @endisset
    <div class="form-group row">
        <label for="maker_car_id" class="col-md-12 col-form-label">Maker Car</label>
        <div class="col-md-12">
            <select name="maker_car_id" id="maker_car_id" class="form-control select2" autofocus>
                <option disabled @if(!isset($item)) selected @endif>Select item</option>
                @foreach ($makers as $value)
                <option value="{{$value->id}}">{{__($value->name)}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="name" class="col-md-12 col-form-label">{{ __('Name Class') }}</label>
        <div class="col-md-12">
            <input id="name" type="text" class="form-control" name="name" required autofocus placeholder="Name Category"
                value="{{$item->name ?? ''}}">
        </div>
    </div>
</form>
@isset($item)
<script>
    $("#maker_car_id").val({{$item->maker_car_id}});
</script>
@endisset