@extends('layouts.admin')
@section('title','ACCESSORY MANAGER')
@section('header_page','List Accessory Page')
@push('head')
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<!-- Toastr -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/toastr/toastr.min.css')}}">
@endpush
@push('script')
<script src="{{ asset('AdminLTE/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('AdminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('AdminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<!-- Toastr -->
<script src="{{ asset('AdminLTE/plugins/toastr/toastr.min.js')}}"></script>
<script>
    $("#table_1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $(".remove").click((e)=>{
        let id = $(e.target).data('id');
        let url = '/dashboard/accessory/'+id;
        $('#form_destroy').attr('action',url);
        $("#Modal_1").modal('show');
    });
</script>
@if (session()->get('create_status'))
<script>
    toastr.options = { "positionClass": "toast-bottom-right"};
    toastr["success"]("Created Success!");
</script>
@endif
@if (session()->get('remove_status'))
<script>
    toastr.options = { "positionClass": "toast-bottom-right"};
    toastr["success"]("Removed Success!");
</script>
@endif
@if (session()->get('edit_status'))
<script>
    toastr.options = { "positionClass": "toast-bottom-right"};
        toastr["success"]("Edited Success!");
</script>
@endif
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Datatable Accessory</h3>
                </div>
                <div class="card-body">
                    <p><a href="{{route('dashboard.accessory.create')}}" class="btn btn-primary">Create</a></p>
                    <table class="table table-bordered" id="table_1">
                        <thead>
                            <tr>
                                <th scope="col">Name Product</th>
                                <th scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Manufacturing Date</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <td>{{$item->name}}</td>
                                <td>{{$item->category->name}}</td>
                                <td>{{$item->showPrice()}}</td>
                                <td>{{$item->brand}}</td>
                                <td>{{$item->manufacturing_date}}</td>
                                <td>
                                    <a href="{{ route('dashboard.accessory.show',['item'=>$item])}}"
                                        class=" btn btn-info">
                                        Detail</a>
                                    <a href="{{ route('dashboard.accessory.edit',['item'=>$item])}}"
                                        class="btn btn-warning"> Edit </a>
                                    <button class="btn btn-danger remove" data-id="{{$item->id}}">Remove</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">Name Product</th>
                                <th scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Manufacturing Date</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="" method="post" id="form_destroy">
    @method('Delete')
    @csrf
</form>
@include('component.modal',['title'=> 'Confirm Destroy', 'btnName'=> 'Oke','id_form'=>'form_destroy','content' => 'Are
you sure?'])
@endsection