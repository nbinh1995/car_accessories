@extends('layouts.admin')
@section('title','ACCESSORY MANAGER')
@section('header_page','Accessory Create Page')
@push('head')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/select2/css/select2.min.css')}}">
<!-- summernote -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/summernote/summernote-bs4.css')}}">
<!-- Toastr -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/toastr/toastr.min.css')}}">
<style>
    .removeType {
        cursor: pointer;
        transition: transform 0.2s ease-in-out;
        transform: scale(1.1)
    }

    .removeType:hover {
        transition: transform 0.2s ease-in-out;
        transform: scale(1)
    }
</style>
@endpush
@push('script')
<!-- Select2 -->
<script src="{{ asset('AdminLTE/plugins/select2/js/select2.full.min.js')}}"></script>
<!-- Summernote -->
<script src="{{ asset('AdminLTE/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- Toastr -->
<script src="{{ asset('AdminLTE/plugins/toastr/toastr.min.js')}}"></script>
<!-- jquery-validation -->
<script src="{{ asset('AdminLTE/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/get_type_car.js')}}"></script>
<script src="{{asset('js/object.js')}}"></script>
<script>
    $('.select2').select2();
    $('.select1').select2({tags: true});
    $('.textarea').summernote();
 
    document.addEventListener("change", function (e) {
    if (e.target && e.target.id == "maker_car_id") {
        let id = e.target.value;
        getClass(id);
    }
    if (e.target && e.target.id == "class_car_id") {
        let id = e.target.value;
        getModel(id);
    }
    });
    $('.addType').click((e)=>{
        getModal();
    });
</script>
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Accessory Create</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('dashboard.accessory.store')}}" method="POST" id="form_1"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-12 col-form-label">{{ __('Name Product') }}</label>
                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" required autofocus placeholder="Name Product">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="category_id"
                                class="col-md-12 col-form-label">{{ __('Category Product') }}</label>
                            <div class="col-md-12">
                                <select name="category_id" id="category_id"
                                    class="form-control select2 @error('category_id') is-invalid @enderror" required
                                    autofocus>
                                    <option disabled selected>Select Category</option>
                                    @foreach ($categories as $value)
                                    <option value="{{$value->id}}">{{__($value->name)}}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-6">
                                <div class="container">
                                    <div class="row">
                                        <label for="manufacturing_date"
                                            class="col-md-12 col-form-label">{{ __('Manufacturing Date') }}</label>
                                        <div class="col-12">
                                            <input type="date" name="manufacturing_date" id="manufacturing_date"
                                                class="form-control @error('manufacturing_date') is-invalid @enderror"
                                                required>
                                            @error('manufacturing_date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="container">
                                    <div class="row">
                                        <label for="price"
                                            class="col-md-12 col-form-label">{{ __('Price Product') }}</label>
                                        <div class="col-12">
                                            <input type="number" name="price" id="price"
                                                class="form-control @error('price') is-invalid @enderror" required>
                                            @error('price')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="brand" class="col-md-12 col-form-label">{{ __('Brand Product') }}</label>

                            <div class="col-md-12">
                                <input id="brand" type="text" class="form-control @error('brand') is-invalid @enderror"
                                    name="brand" required autofocus placeholder="Brand Product">

                                @error('brand')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <button type="button" class="btn btn-info addType">Type Car</button>
                        <input type="hidden" name="arr_model" id="arr_model" value="">
                        <div class="form-group row tagType">
                        </div>
                        <div class="form-group row">
                            <label for="description"
                                class="col-md-12 col-form-label">{{ __('Description Product') }}</label>

                            <div class="col-md-12">
                                <textarea class="textarea form-control  @error('description') is-invalid @enderror"
                                    placeholder="Place some text here"
                                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                    name="description" id="description"></textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <input type="submit" value="Add Product" class="btn btn-primary px-5 py-2">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('component.modal',['title'=>'Add Type','btnName'=> 'Add Type','id_form'=>'form_addobject','size_model' =>
'modal-lg'])
@endsection