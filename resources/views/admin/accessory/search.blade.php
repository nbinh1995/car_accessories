@extends('layouts.admin')
@section('title','ACCESSORY MANAGER')
@section('header_page','Accessory Search Page')
@push('head')
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/select2/css/select2.min.css')}}">
<!-- Toastr -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/toastr/toastr.min.css')}}">
@endpush
@push('script')
<script src="{{ asset('AdminLTE/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('AdminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('AdminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<!-- Select2 -->
<script src="{{ asset('AdminLTE/plugins/select2/js/select2.full.min.js')}}"></script>
<!-- Toastr -->
<script src="{{ asset('AdminLTE/plugins/toastr/toastr.min.js')}}"></script>
<script src="{{asset('js/search.js')}}"></script>
<script>
    $('.select2').select2();
    $('.select1').select2({tags: true});
    $("#table_3").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
</script>
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Accessory Search</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('dashboard.accessory.search.result')}}" id="form_3">
                        @csrf
                        <div class="form-group row">
                            <div class="col-4">
                                <label for="name" class="col-md-12 col-form-label">{{ __('Name Product') }}</label>
                                <div class="col-md-12">
                                    <input id="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" name="name" autofocus
                                        placeholder="Name Product">
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="category_id"
                                    class="col-md-12 col-form-label">{{ __('Category Product') }}</label>
                                <div class="col-md-12">
                                    <select name="category_id" id="category_id"
                                        class="form-control select2 @error('category_id') is-invalid @enderror"
                                        autofocus>
                                        <option disabled selected>Select Category</option>
                                        @foreach ($categories as $value)
                                        <option value="{{$value->id}}">{{__($value->name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="brand" class="col-md-12 col-form-label">{{ __('Brand Product') }}</label>

                                <div class="col-md-12">
                                    <input id="brand" type="text"
                                        class="form-control @error('brand') is-invalid @enderror" name="brand" autofocus
                                        placeholder="Brand Product">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="manufacturing_date"
                                    class="col-md-12 col-form-label">{{ __('Manufacturing Date') }}</label>
                                <div class="col-12">
                                    <input type="date" name="manufacturing_date_from" id="manufacturing_date_from"
                                        class="form-control">
                                </div>
                            </div>
                            <h1 class="align-self-end text-muted">~</h1>
                            <div class="col-5 align-self-end">
                                <div class="col-12">
                                    <input type="date" name="manufacturing_date_to" id="manufacturing_date_to"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-5">
                                <label class="col-md-12 col-form-label">{{ __('Price Product') }}</label>
                                <div class="col-12">
                                    <input type="number" name="price_from" id="price_from" class="form-control">
                                </div>
                            </div>
                            <h1 class="align-self-end text-muted">~</h1>
                            <div class="col-5 align-self-end">
                                <input type="number" name="price_to" id="price_to" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="model_car_id" class="col-md-12 col-form-label">Model Car</label>
                            <div class="col-md-12">
                                <select name="model_car_id[]" id="model_car_id" class="form-control select1"
                                    multiple="multiple" autofocus>
                                    @foreach ($makers as $item)
                                    @foreach ($item->classCars as $obj)
                                    @foreach ($obj->modelCars as $value)
                                    <option value="{{$value->id}}">
                                        {{__($item->name.'__'.$obj->name.'__'.$value->name)}}
                                    </option>
                                    @endforeach
                                    @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <input type="submit" value="Search" class="btn btn-primary px-5 py-2">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-10">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered" id="table_3">
                        <thead>
                            <tr>
                                <th scope="col">Name Product</th>
                                <th scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Manufacturing Date</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection