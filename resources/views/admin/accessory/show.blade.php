@extends('layouts.admin')
@section('title','ACCESSORY MANAGER')
@section('header_page','Accessory Detail Page')
@push('head')
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<!-- Toastr -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/toastr/toastr.min.css')}}">
@endpush
@push('script')
<script src="{{ asset('AdminLTE/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('AdminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('AdminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<!-- Toastr -->
<script src="{{ asset('AdminLTE/plugins/toastr/toastr.min.js')}}"></script>
<!-- jquery-validation -->
<script src="{{ asset('AdminLTE/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/get_type_car.js')}}"></script>
<script src="{{asset('js/edit_modal.js')}}"></script>
<script>
    var product = "<input type='hidden' name='accessory_id' value='{{$item->id}}' form='form_addobject'>";
    $("#table_2").DataTable({
      "responsive": true,
      "autoWidth": false,
    });

    $(".remove").click((e)=>{
        let id = $(e.target).data('id');
        let url = '/dashboard/useObject/'+id;
        editModal('Confirm Destroy','Oke','modal-md','form_destroy','Are You Sure?');
        $('#form_destroy').attr('action',url);
        $("#Modal_1").modal('show');
    });

    $(".create").click((e)=>{
        editModal('Add Type','Add Type','modal-lg','form_addobject','');
        $('#Modal_1').append(product);
        getModal();
    });

    document.addEventListener("change", function (e) {
    if (e.target && e.target.id == "maker_car_id") {
        let id = e.target.value;
        getClass(id);
    }
    if (e.target && e.target.id == "class_car_id") {
        let id = e.target.value;
        getModel(id);
    }
});
</script>
@if (session()->get('remove_status'))
<script>
    toastr.options = { "positionClass": "toast-bottom-right"};
    toastr["success"]("Removed Success!");
</script>
@endif
@if (session()->get('add_status'))
<script>
    toastr.options = { "positionClass": "toast-bottom-right"};
        toastr["success"]("Created Success!");
</script>
@endif
@endpush
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <h5><ins>Name Product:</ins> <span
                                        class="text-muted text-capitalize">{{$item->name}}</span> </h5>
                                <h5><ins>Category Produc:</ins> <span
                                        class="text-muted text-capitalize">{{$item->category->name}}</span>
                                </h5>
                                <h5><ins>Brand Product:</ins> <span
                                        class="text-muted text-capitalize">{{$item->brand}}</span>
                                </h5>
                                <h5><ins>Price:</ins> <span
                                        class="text-muted text-capitalize">{{$item->showPrice()}}</span></h5>
                                <h5><ins>Manufacturing Date:</ins> <span
                                        class="text-muted text-capitalize">{{$item->manufacturing_date}}</span></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <h5><ins>Description: </ins></h5>
                            </div>
                            <div class="col-12">
                                <p class="text-muted">{{$item->description}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <h3><ins>Type Car Of Accessory</ins></h3>
                            </div>
                            <div class="col-12 my-3">
                                <button class="btn btn-primary create">Create</button>
                            </div>
                            <div class="col-12">
                                <table class="table table-bordered" id="table_2">
                                    <thead>
                                        <tr>
                                            <th scope="col">Maker Car</th>
                                            <th scope="col">Class Car</th>
                                            <th scope="col">Model Car</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($object as $item)
                                        <tr>
                                            <td>{{$item->modelCar->classCar->makerCar->name ?? 'NULL'}}</td>
                                            <td>{{$item->modelCar->classCar->name ?? 'NULL'}}</td>
                                            <td>{{$item->modelCar->name ?? 'NULL'}}</td>
                                            <td><button class="btn btn-danger remove"
                                                    data-id="{{$item->id}}">Remove</button></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th scope="col">Maker Car</th>
                                            <th scope="col">Class Car</th>
                                            <th scope="col">Model Car</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="" method="post" id="form_destroy">
    @method('Delete')
    @csrf
</form>
@include('component.modal')
@endsection