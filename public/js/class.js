var class_ = class_ || {};

let path = location.origin;

class_.showTable = function () {
    let url = path + "/dashboard/class/showTable";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            console.log(data);
            table.clear().draw();
            $.each(data.data, function (i, v) {
                table.row
                    .add([
                        v.maker_car.name,
                        v.name,
                        `<button class="btn btn-info edit" data-id='${v.id}'>Edit</button>
                        <button class="btn btn-danger remove" data-id='${v.id}'>remove</button>`,
                    ])
                    .draw();
            });
        },
    });
};

class_.storeRow = function (ele) {
    let url = $(ele).attr("action");
    let data = new FormData(ele);
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data["code"] == 200) {
                class_.showTable();
                toastr.options = { positionClass: "toast-bottom-right" };
                toastr["success"]("Created Success!");
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        },
    });
};

class_.editRow = function (id) {
    let url = path + `/dashboard/class/${id}/edit`;
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            setForm(data.data);
        },
    });
};

class_.updateRow = function (ele) {
    let url = $(ele).attr("action");
    let data = new FormData(ele);
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data["code"] == 200) {
                class_.showTable();
                toastr.options = { positionClass: "toast-bottom-right" };
                toastr["success"]("Updated Success!");
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        },
    });
};

class_.destroyRow = function (id) {
    let token = $("meta[name='csrf-token']").attr("content");
    let url = path + `/dashboard/class/${id}`;
    bootbox.confirm({
        message: "Bạn có chắc chắn?",
        buttons: {
            confirm: {
                label: "Có",
                className: "btn-success",
            },
            cancel: {
                label: "Không",
                className: "btn-danger",
            },
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: {
                        _token: token,
                    },
                    success: function (data) {
                        if (data["code"] == 200) {
                            class_.showTable();
                            toastr.options = {
                                positionClass: "toast-bottom-right",
                            };
                            toastr["success"]("Removed Success!");
                        }
                    },
                });
            }
        },
    });
};

class_.init = function () {
    class_.showTable();
};

$(document).ready(function () {
    class_.init();

    $(document).on("click", ".create", function (e) {
        editModal("Add Modal", "Add Item", "modal-md", "form_create", "");
        setForm(html);
        $("#Modal_1").modal("show");
    });

    $(document).on("click", ".edit", function (e) {
        editModal("Edit Modal", "Update Item", "modal-md", "form_edit", "");
        class_.editRow($(e.target).data("id"));
        $("#Modal_1").modal("show");
    });

    $(document).on("click", ".remove", function (e) {
        class_.destroyRow($(e.target).data("id"));
    });

    $(document).on("submit", "#form_create", function (e) {
        e.preventDefault();
        class_.storeRow(e.target);
        $("#Modal_1").modal("hide");
    });

    $(document).on("submit", "#form_edit", function (e) {
        e.preventDefault();
        class_.updateRow(e.target);
        $("#Modal_1").modal("hide");
    });
});
