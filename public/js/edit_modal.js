function editModal(title, btnName, size_modal, id_form, content) {
    $("#Modal_1").find(".modal-title").text(title);
    $("#Modal_1").find(".modal-dialog").addClass(size_modal);
    $("#Modal_1").find(".modal-body").text(content);
    $("#Modal_1").find("button[type='submit']").text(btnName);
    $("#Modal_1").find("button[type='submit']").attr("form", id_form);
}

function setForm(html) {
    $("#Modal_1").find(".modal-body").html(html);
}
