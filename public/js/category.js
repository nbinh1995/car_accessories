var category = category || {};

let path = location.origin;

category.showTable = function () {
    let url = path + "/dashboard/category/showTable";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            table.clear().draw();
            $.each(data.data, function (i, v) {
                table.row
                    .add([
                        v.name,
                        `<button class="btn btn-info edit" data-id='${v.id}'>Edit</button>
                        <button class="btn btn-danger remove" data-id='${v.id}'>remove</button>`,
                    ])
                    .draw();
            });
        },
    });
};

category.storeRow = function (ele) {
    let url = $(ele).attr("action");
    let data = new FormData(ele);
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data["code"] == 200) {
                category.showTable();
                toastr.options = { positionClass: "toast-bottom-right" };
                toastr["success"]("Created Success!");
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr.responseJSON);
            console.log(status);
            console.log(error);
        },
    });
};

category.editRow = function (id) {
    let url = path + `/dashboard/category/${id}/edit`;
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            setForm(data.data);
        },
    });
};

category.updateRow = function (ele) {
    let url = $(ele).attr("action");
    let data = new FormData(ele);
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data["code"] == 200) {
                category.showTable();
                toastr.options = { positionClass: "toast-bottom-right" };
                toastr["success"]("Updated Success!");
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr.responseJSON);
            console.log(status);
            console.log(error);
        },
    });
};

category.destroyRow = function (id) {
    let token = $("meta[name='csrf-token']").attr("content");
    let url = path + `/dashboard/category/${id}`;
    bootbox.confirm({
        message: "Bạn có chắc chắn?",
        buttons: {
            confirm: {
                label: "Có",
                className: "btn-success",
            },
            cancel: {
                label: "Không",
                className: "btn-danger",
            },
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: {
                        _token: token,
                    },
                    success: function (data) {
                        if (data["code"] == 200) {
                            category.showTable();
                            toastr.options = {
                                positionClass: "toast-bottom-right",
                            };
                            toastr["success"]("Removed Success!");
                        }
                    },
                });
            }
        },
    });
};

category.init = function () {
    category.showTable();
};

$(document).ready(function () {
    category.init();

    $(document).on("click", ".create", function (e) {
        editModal("Add Modal", "Add Item", "modal-md", "form_create", "");
        setForm(html);
        $("#Modal_1").modal("show");
    });

    $(document).on("click", ".edit", function (e) {
        editModal("Edit Modal", "Update Item", "modal-md", "form_edit", "");
        category.editRow($(e.target).data("id"));
        $("#Modal_1").modal("show");
    });

    $(document).on("click", ".remove", function (e) {
        category.destroyRow($(e.target).data("id"));
    });

    $(document).on("submit", "#form_create", function (e) {
        e.preventDefault();
        category.storeRow(e.target);
        $("#Modal_1").modal("hide");
    });

    $(document).on("submit", "#form_edit", function (e) {
        e.preventDefault();
        category.updateRow(e.target);
        $("#Modal_1").modal("hide");
    });
});
