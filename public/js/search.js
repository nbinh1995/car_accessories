$("#form_3").submit(function (e) {
    e.preventDefault();
    let data = new FormData(e.target);
    $.ajax({
        url: "/dashboard/accessory/search",
        method: "post",
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            // console.log(data.data);
            if (data.code == 200) {
                var list_table = $("#table_3").DataTable();
                list_table.clear().draw();
                $.each(data.data, function (index, value) {
                    list_table.row
                        .add([
                            value.name,
                            value.category.name,
                            value.price,
                            value.brand,
                            value.manufacturing_date,
                            `<a href="/dashboard/accessory/${value.id}"
                            class=" btn btn-info">
                            Detail</a>`,
                        ])
                        .draw();
                });
            }
        },
    });
});
