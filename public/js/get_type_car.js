function getModal() {
    fetch("/dashboard/getmaker", {
        method: "get",
    })
        .then(function (response) {
            if (response.status !== 200) {
                return;
            }

            // // Examine the text in the response
            response.json().then(function (data) {
                let html = data.data;
                document
                    .getElementById("Modal_1")
                    .querySelector(".modal-body").innerHTML = html;
                $("#Modal_1").modal("show");
            });
        })
        .catch(function (err) {
            console.log("Fetch Error :-S", err);
        });
}

function getClass(id) {
    fetch("/dashboard/getclass/" + id, {
        method: "get",
    })
        .then(function (response) {
            if (response.status !== 200) {
                return;
            }

            // // Examine the text in the response
            response.json().then(function (data) {
                let html = data.data;
                document.getElementById("class_car_id").innerHTML = html;
                document.getElementById("model_car_id").innerHTML =
                    "<option disabled selected>Select Item</option>";
            });
        })
        .catch(function (err) {
            console.log("Fetch Error :-S", err);
        });
}

function getModel(id) {
    fetch("/dashboard/getmodel/" + id, {
        method: "get",
    })
        .then(function (response) {
            if (response.status !== 200) {
                return;
            }

            // // Examine the text in the response
            response.json().then(function (data) {
                let html = data.data;
                document.getElementById("model_car_id").innerHTML = html;
            });
        })
        .catch(function (err) {
            console.log("Fetch Error :-S", err);
        });
}
