var object = object || {};

object.showList = function () {
    let arr_model = $("#arr_model").val();
    let token = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url: "/dashboard/getObject",
        method: "post",
        data: {
            _token: token,
            arr_model: arr_model,
        },
        dataType: "json",
        success: function (data) {
            $(".tagType").empty();
            $(".tagType").html(data.data);
        },
    });
};

object.addList = function (ele) {
    if ($(ele).find("#model_car_id").val()) {
        let arr_model = $("#arr_model").val()
            ? $("#arr_model").val().split(",")
            : [];
        arr_model.push($(ele).find("#model_car_id").val());
        $("#arr_model").val(arr_model.join(","));
        object.showList();
        $("#Modal_1").modal("hide");
    } else {
    }
};

object.removeList = function (ele) {
    let id = $(ele).data("id");
    let arr_model = $("#arr_model").val().split(",");
    arr_model_new = arr_model.filter((v) => {
        return v != id;
    });
    $("#arr_model").val(arr_model_new.join(","));
    object.showList();
};

object.init = function () {
    object.showList();
};

$(document).ready(function () {
    object.init();

    $(document).on("submit", "#form_addobject", function (e) {
        e.preventDefault();
        object.addList(e.target);
    });

    $(document).on("click", ".removeType", function (e) {
        object.removeList(e.target);
    });
});
