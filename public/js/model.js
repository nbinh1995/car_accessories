var model = model || {};

let path = location.origin;

model.showTable = function () {
    // console.log("alo");
    let url = path + "/dashboard/model/showTable";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            // console.log(data.data);
            table.clear().draw();
            $.each(data.data, function (i, v) {
                table.row
                    .add([
                        v.class_car.maker_car.name,
                        v.class_car.name,
                        v.name,
                        `<button class="btn btn-info edit" data-id='${v.id}'>Edit</button>
                        <button class="btn btn-danger remove" data-id='${v.id}'>remove</button>`,
                    ])
                    .draw();
            });
        },
    });
};

model.getClass = function (id) {
    let url = path + `/dashboard/getclass/${id}`;
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            $("#class_car_id").html(data.data);
        },
    });
};

model.storeRow = function (ele) {
    let url = $(ele).attr("action");
    let data = new FormData(ele);
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data["code"] == 200) {
                model.showTable();
                toastr.options = { positionClass: "toast-bottom-right" };
                toastr["success"]("Created Success!");
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        },
    });
};

model.editRow = function (id) {
    let url = path + `/dashboard/model/${id}/edit`;
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            setForm(data.data);
        },
    });
};

model.updateRow = function (ele) {
    let url = $(ele).attr("action");
    let data = new FormData(ele);
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data["code"] == 200) {
                model.showTable();
                toastr.options = { positionClass: "toast-bottom-right" };
                toastr["success"]("Updated Success!");
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        },
    });
};

model.destroyRow = function (id) {
    let token = $("meta[name='csrf-token']").attr("content");
    let url = path + `/dashboard/model/${id}`;
    bootbox.confirm({
        message: "Bạn có chắc chắn?",
        buttons: {
            confirm: {
                label: "Có",
                className: "btn-success",
            },
            cancel: {
                label: "Không",
                className: "btn-danger",
            },
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: {
                        _token: token,
                    },
                    success: function (data) {
                        if (data["code"] == 200) {
                            model.showTable();
                            toastr.options = {
                                positionClass: "toast-bottom-right",
                            };
                            toastr["success"]("Removed Success!");
                        }
                    },
                });
            }
        },
    });
};

model.init = function () {
    model.showTable();
};

$(document).ready(function () {
    model.init();

    $(document).on("click", ".create", function (e) {
        editModal("Add Modal", "Add Item", "modal-md", "form_create", "");
        setForm(html);
        $("#Modal_1").modal("show");
    });

    $(document).on("click", ".edit", function (e) {
        editModal("Edit Modal", "Update Item", "modal-md", "form_edit", "");
        model.editRow($(e.target).data("id"));
        $("#Modal_1").modal("show");
    });

    $(document).on("click", ".remove", function (e) {
        model.destroyRow($(e.target).data("id"));
    });

    $(document).on("submit", "#form_create", function (e) {
        e.preventDefault();
        model.storeRow(e.target);
        $("#Modal_1").modal("hide");
    });

    $(document).on("submit", "#form_edit", function (e) {
        e.preventDefault();
        model.updateRow(e.target);
        $("#Modal_1").modal("hide");
    });
});
