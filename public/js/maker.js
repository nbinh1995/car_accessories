var maker = maker || {};

let path = location.origin;

maker.showTable = function () {
    let url = path + "/dashboard/maker/showTable";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            table.clear().draw();
            $.each(data.data, function (i, v) {
                table.row
                    .add([
                        v.name,
                        `<button class="btn btn-info edit" data-id='${v.id}'>Edit</button>
                        <button class="btn btn-danger remove" data-id='${v.id}'>remove</button>`,
                    ])
                    .draw();
            });
        },
    });
};

maker.storeRow = function (ele) {
    let url = $(ele).attr("action");
    let data = new FormData(ele);
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data["code"] == 200) {
                maker.showTable();
                toastr.options = { positionClass: "toast-bottom-right" };
                toastr["success"]("Created Success!");
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        },
    });
};

maker.editRow = function (id) {
    let url = path + `/dashboard/maker/${id}/edit`;
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            setForm(data.data);
        },
    });
};

maker.updateRow = function (ele) {
    let url = $(ele).attr("action");
    let data = new FormData(ele);
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data["code"] == 200) {
                maker.showTable();
                toastr.options = { positionClass: "toast-bottom-right" };
                toastr["success"]("Updated Success!");
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        },
    });
};

maker.destroyRow = function (id) {
    let token = $("meta[name='csrf-token']").attr("content");
    let url = path + `/dashboard/maker/${id}`;
    bootbox.confirm({
        message: "Bạn có chắc chắn?",
        buttons: {
            confirm: {
                label: "Có",
                className: "btn-success",
            },
            cancel: {
                label: "Không",
                className: "btn-danger",
            },
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: {
                        _token: token,
                    },
                    success: function (data) {
                        if (data["code"] == 200) {
                            maker.showTable();
                            toastr.options = {
                                positionClass: "toast-bottom-right",
                            };
                            toastr["success"]("Removed Success!");
                        }
                    },
                });
            }
        },
    });
};

maker.init = function () {
    maker.showTable();
};

$(document).ready(function () {
    maker.init();

    $(document).on("click", ".create", function (e) {
        editModal("Add Modal", "Add Item", "modal-md", "form_create", "");
        setForm(html);
        $("#Modal_1").modal("show");
    });

    $(document).on("click", ".edit", function (e) {
        editModal("Edit Modal", "Update Item", "modal-md", "form_edit", "");
        maker.editRow($(e.target).data("id"));
        $("#Modal_1").modal("show");
    });

    $(document).on("click", ".remove", function (e) {
        maker.destroyRow($(e.target).data("id"));
    });

    $(document).on("submit", "#form_create", function (e) {
        e.preventDefault();
        maker.storeRow(e.target);
        $("#Modal_1").modal("hide");
    });

    $(document).on("submit", "#form_edit", function (e) {
        e.preventDefault();
        maker.updateRow(e.target);
        $("#Modal_1").modal("hide");
    });
});
