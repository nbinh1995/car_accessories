<?php

use App\Http\Controllers\Admin\AccessoryController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ClassCarController;
use App\Http\Controllers\Admin\MakerCarController;
use App\Http\Controllers\Admin\ModelCarController;
use App\Http\Controllers\Admin\UseObjectController;
use App\Http\Controllers\Admin\DashboardController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', DashboardController::class)->middleware('isAdmin');

// Admin Route

Route::group([
    'prefix' => 'dashboard',
    'middleware' => 'isAdmin'
], function () {

    // DashboardController

    Route::get('/', DashboardController::class)->name('dashboard');

    // AccessoryController

    Route::get('/accessory', [AccessoryController::class, 'index'])->name('dashboard.accessory.index');
    Route::get('/accessory/search', [AccessoryController::class, 'search'])->name('dashboard.accessory.search');
    Route::post('/accessory/search', [AccessoryController::class, 'searchResult'])->name('dashboard.accessory.search.result');
    Route::get('/accessory/create', [AccessoryController::class, 'create'])->name('dashboard.accessory.create');
    Route::post('/accessory', [AccessoryController::class, 'store'])->name('dashboard.accessory.store');
    Route::get('/accessory/{item}', [AccessoryController::class, 'show'])->name('dashboard.accessory.show');
    Route::get('/accessory/{item}/edit', [AccessoryController::class, 'edit'])->name('dashboard.accessory.edit');
    Route::patch('/accessory/{item}', [AccessoryController::class, 'update'])->name('dashboard.accessory.update');
    Route::delete('/accessory/{item}', [AccessoryController::class, 'destroy'])->name('dashboard.accessory.destroy');

    // UseObjectController

    Route::post('/getObject', [UseObjectController::class, 'getObject'])->name('dashboard.userObject.get');
    Route::post('/useObject', [UseObjectController::class, 'store'])->name('dashboard.userObject.store');
    Route::delete('/useObject/{item}', [UseObjectController::class, 'destroy'])->name('dashboard.userObject.destroy');

    // CategoryController

    Route::get('/category', [CategoryController::class, 'index'])->name('dashboard.category.index');
    Route::get('/category/showTable', [CategoryController::class, 'showTable'])->name('dashboard.category.showTable');
    Route::post('/category/showTable', [CategoryController::class, 'store'])->name('dashboard.category.store');
    Route::get('/category/{item}/edit', [CategoryController::class, 'edit'])->name('dashboard.category.edit');
    Route::patch('/category/{item}', [CategoryController::class, 'update'])->name('dashboard.category.update');
    Route::delete('/category/{item}', [CategoryController::class, 'destroy'])->name('dashboard.category.destroy');

    // MakerCarController

    Route::get('/getmaker', [MakerCarController::class, 'getMaker'])->name('dashboard.maker.getMaker');
    Route::get('/maker', [MakerCarController::class, 'index'])->name('dashboard.maker.index');
    Route::get('/maker/showTable', [MakerCarController::class, 'showTable'])->name('dashboard.maker.showTable');
    Route::post('/maker', [MakerCarController::class, 'store'])->name('dashboard.maker.store');
    Route::get('/maker/{item}/edit', [MakerCarController::class, 'edit'])->name('dashboard.maker.edit');
    Route::patch('/maker/{item}', [MakerCarController::class, 'update'])->name('dashboard.maker.update');
    Route::delete('/maker/{item}', [MakerCarController::class, 'destroy'])->name('dashboard.maker.destroy');

    // ClassCarController

    Route::get('/getclass/{id}', [ClassCarController::class, 'getClass'])->name('dashboard.class.getClass');
    Route::get('/class', [ClassCarController::class, 'index'])->name('dashboard.class.index');
    Route::get('/class/showTable', [ClassCarController::class, 'showTable'])->name('dashboard.class.showTable');
    Route::post('/class', [ClassCarController::class, 'store'])->name('dashboard.class.store');
    Route::get('/class/{item}/edit', [ClassCarController::class, 'edit'])->name('dashboard.class.edit');
    Route::patch('/class/{item}', [ClassCarController::class, 'update'])->name('dashboard.class.update');
    Route::delete('/class/{item}', [ClassCarController::class, 'destroy'])->name('dashboard.class.destroy');

    // ModelCarController

    Route::get('/getmodel/{id}', [ModelCarController::class, 'getModel'])->name('dashboard.model.getModel');
    Route::get('/model', [ModelCarController::class, 'index'])->name('dashboard.model.index');
    Route::get('/model/showTable', [ModelCarController::class, 'showTable'])->name('dashboard.model.showTable');
    Route::post('/model', [ModelCarController::class, 'store'])->name('dashboard.model.store');
    Route::get('/model/{item}/edit', [ModelCarController::class, 'edit'])->name('dashboard.model.edit');
    Route::patch('/model/{item}', [ModelCarController::class, 'update'])->name('dashboard.model.update');
    Route::delete('/model/{item}', [ModelCarController::class, 'destroy'])->name('dashboard.model.destroy');
});
