<?php

namespace App\Http\ViewComposers;

use App\Models\ClassCar;
use App\Models\MakerCar;
use App\Models\ModelCar;
use Illuminate\View\View;

class CarComposer
{
    protected $makers;

    public function __construct()
    {
        $this->makers = MakerCar::with('classCars.modelCars')->get();
    }

    public function compose(View $view)
    {
        $view->with('makers', $this->makers);
    }
}
