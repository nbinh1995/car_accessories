<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class isAdmin
{
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            if (auth()->user()->isAdmin()) {
                return $next($request);
            }
            Auth::logout();
            $request->session()->flush();
            return redirect('/login')->withFlashDanger(__('Not_Admin'));
        }
        return redirect('/login')->withFlashDanger(__('Unauthenticated'));
    }
}
