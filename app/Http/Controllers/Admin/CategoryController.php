<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $url = route('dashboard.category.store');
        $html = view('partials.form-category', ['url' => $url, 'type' => 'form_create'])->render();

        return view('admin.category.index', ['html' => $html]);
    }

    public function showTable()
    {
        $item = Category::orderBy('updated_at', 'DESC')->get();

        return  response()->json(['data' => $item, 'code' => 200], 200);
    }

    public function store(CategoryRequest $request)
    {
        $data = Category::create($request->all());

        return response()->json(['code' => 200, 'data' => $data], 200);
    }

    public function edit(Category $item)
    {
        $url = route('dashboard.category.update', ['item' => $item]);
        $html = view('partials.form-category', ['url' => $url, 'type' => 'form_edit', 'item' => $item])->render();

        return response()->json(['data' => $html, 'code' => 200], 200);
    }

    public function update(CategoryRequest $request, Category $item)
    {
        $item->update($request->all());
        return response()->json(['data' => $item, 'code' => 200], 200);
    }

    public function destroy(Category $item)
    {
        $item->delete();
        return response()->json(['code' => 200], 200);
    }
}
