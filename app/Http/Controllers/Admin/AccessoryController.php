<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AccessoryRequest;
use App\Models\Accessory;
use App\Models\UseObject;
use Illuminate\Http\Request;

class AccessoryController extends Controller
{

    public function index()
    {

        $items =  Accessory::orderBy('updated_at', 'DESC')->with('category:id,name')->get();

        return view('admin.accessory.index', compact('items'));
    }

    public function create()
    {
        return view('admin.accessory.create');
    }

    public function search()
    {
        return view('admin.accessory.search');
    }

    public function searchResult(Request $request)
    {

        // dump($request->all());
        // DB::connection()->enableQueryLog();
        $result = Accessory::with('category:id,name')
            ->whereHas('use_objects', function ($query) use ($request) {
                $query->orWhereIn('model_car_id', $request->model_car_id);
                return $query;
            });

        if ($request->name) {
            $result->where('name', 'like', "%{$request->name}%");
        }
        if ($request->category_id) {
            $result->where('category_id', $request->category_id);
        }
        if ($request->brand) {
            $result->where('brand', 'like', "%{$request->brand}%");
        }

        if ($request->manufacturing_date_from) {
            $result->where('manufacturing_date', '>=', $request->manufacturing_date_from);
        }
        if ($request->manufacturing_date_to) {
            $result->where('manufacturing_date', '<=', $request->manufacturing_date_to);
        }
        if ($request->price_from) {
            $result->where('price', '>=', $request->price_from);
        }
        if ($request->price_to) {
            $result->where('price', '<=', $request->price_to);
        }
        $data = $result->get();
        // $queries = DB::getQueryLog();
        return response()->json(['data' => $data, 'code' => 200], 200);
    }

    public function store(AccessoryRequest $request)
    {
        $arrModel = [];
        if ($request->arr_model) {
            $arrModel = explode(',', $request->arr_model);
        }
        $accessory = Accessory::create($request->except('arr_model'));
        foreach ($arrModel as $item) {
            UseObject::create(['accessory_id' => $accessory->id, 'model_car_id' => $item]);
        }

        return redirect()->route('dashboard.accessory.index')->with('create_status', 'Success');;
    }

    public function show(Accessory $item)
    {
        $object = UseObject::with('modelCar.classCar.makerCar')->where('accessory_id', $item->id)->get();

        return view('admin.accessory.show', compact('item', 'object'));
    }

    public function edit(Accessory $item)
    {
        $object = UseObject::where('accessory_id', $item->id)->get();
        $arr = [];
        foreach ($object as $value) {
            $arr[] = $value->model_car_id;
        }
        $json = implode(',', $arr);
        return view('admin.accessory.edit', compact('item', 'json'));
    }

    public function update(AccessoryRequest $request, Accessory $item)
    {
        $arrModel = [];
        if ($request->arr_model) {
            $arrModel = explode(',', $request->arr_model);
        }

        $arrObj = $item->use_objects;
        $item->update($request->except('arr_model'));

        $accessory_id = $item->id;
        foreach ($arrObj as $obj) {
            $obj->delete();
        }

        foreach ($arrModel as $model_car_id) {
            UseObject::create(['accessory_id' => $accessory_id, 'model_car_id' => $model_car_id]);
        }
        return redirect()->route('dashboard.accessory.index')->with('edit_status', 'Success');
    }

    public function destroy(Accessory $item)
    {
        $item->delete();
        return redirect()->route('dashboard.accessory.index')->with('remove_status', 'Success');
    }
}
