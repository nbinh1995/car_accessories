<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ModelCarRequest;
use App\Models\ModelCar;

class ModelCarController extends Controller
{
    public function getModel($id)
    {
        $model = ModelCar::where('class_car_id', $id)->get();
        $html = view('component.select', ['items' => $model])->render();

        return response()->json(['data' => $html], 200);
    }

    public function index()
    {
        $item = ModelCar::orderBy('updated_at', 'DESC')->with('classCar.makerCar')->get();
        $url = route('dashboard.model.store');
        $html = view('partials.form-model', ['url' => $url, 'type' => 'form_create'])->render();

        return view('admin.model.index', ['html' => $html]);
    }

    public function showTable()
    {
        $item = ModelCar::orderBy('updated_at', 'DESC')->with('classCar.makerCar')->get();

        return  response()->json(['data' => $item, 'code' => 200], 200);
    }

    public function store(ModelCarRequest $request)
    {
        $data = ModelCar::create($request->except('maker_car_id'));

        return response()->json(['code' => 200, 'data' => $data], 200);
    }

    public function edit(ModelCar $item)
    {

        $url = route('dashboard.model.update', ['item' => $item]);
        $html = view('partials.form-model', ['url' => $url, 'type' => 'form_edit', 'item' => $item])->render();

        return response()->json(['data' => $html, 'code' => 200], 200);
    }

    public function update(ModelCarRequest $request, ModelCar $item)
    {
        $item->update($request->except('maker_car_id'));

        return response()->json(['data' => $item, 'code' => 200], 200);
    }

    public function destroy(ModelCar $item)
    {
        $item->delete();

        return response()->json(['code' => 200], 200);
    }
}
