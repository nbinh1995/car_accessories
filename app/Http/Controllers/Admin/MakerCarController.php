<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MakerCarRequest;
use App\Models\MakerCar;

class MakerCarController extends Controller
{
    public function getMaker()
    {
        $makers = MakerCar::all();
        $html = view('partials.form-add_object', compact('makers'))->render();

        return response()->json(['data' => $html], 200);
    }

    public function index()
    {
        $item = MakerCar::orderBy('updated_at', 'DESC')->get();
        $url = route('dashboard.maker.store');
        $html = view('partials.form-maker', ['url' => $url, 'type' => 'form_create'])->render();

        return view('admin.maker.index', ['html' => $html]);
    }

    public function showTable()
    {
        $item = MakerCar::orderBy('updated_at', 'DESC')->get();

        return  response()->json(['data' => $item, 'code' => 200], 200);
    }

    public function store(MakerCarRequest $request)
    {
        $data = MakerCar::create($request->all());

        return response()->json(['code' => 200, 'data' => $data], 200);
    }

    public function edit(MakerCar $item)
    {
        $url = route('dashboard.maker.update', ['item' => $item]);
        $html = view('partials.form-maker', ['url' => $url, 'type' => 'form_edit', 'item' => $item])->render();

        return response()->json(['data' => $html, 'code' => 200], 200);
    }

    public function update(MakerCarRequest $request, MakerCar $item)
    {
        $item->update($request->all());

        return response()->json(['data' => $item, 'code' => 200], 200);
    }

    public function destroy(MakerCar $item)
    {
        $item->delete();

        return response()->json(['code' => 200], 200);
    }
}
