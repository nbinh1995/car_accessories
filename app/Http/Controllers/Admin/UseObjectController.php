<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ClassCar;
use App\Models\MakerCar;
use App\Models\ModelCar;
use App\Models\UseObject;
use Illuminate\Http\Request;

use function GuzzleHttp\Promise\all;

class UseObjectController extends Controller
{
    public function getObject(Request $request)
    {
        $arrModel = [];
        if ($request->arr_model) {
            $arrModel = explode(',', $request->arr_model);
        }
        $html = '';
        if ($arrModel) {
            foreach ($arrModel as $id) {
                $item = ModelCar::with('classCar.makerCar')->findOrFail($id);
                $html .= view('component.tagModel', ['item' => $item])->render();
            }
        }

        return response()->json(['data' => $html, 'code' => 200], 200);
    }

    public function store(Request $request)
    {
        UseObject::create($request->except('maker_car_id', 'class_car_id'));

        return redirect()->back()->with('add_status', 'Success');
    }

    public function destroy(UseObject $item)
    {
        $item->delete();
        return redirect()->back()->with('remove_status', 'Success');
    }
}
