<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClassCarRequest;
use App\Models\ClassCar;

class ClassCarController extends Controller
{
    public function getClass($id)
    {
        $class = ClassCar::where('maker_car_id', $id)->get();
        $html = view('component.select', ['items' => $class])->render();

        return response()->json(['data' => $html], 200);
    }

    public function index()
    {
        $url = route('dashboard.class.store');
        $html = view('partials.form-class', ['url' => $url, 'type' => 'form_create'])->render();

        return view('admin.class.index', ['html' => $html]);
    }

    public function showTable()
    {
        $item = ClassCar::orderBy('updated_at', 'DESC')->with('makerCar:id,name')->get();

        return  response()->json(['data' => $item, 'code' => 200], 200);
    }

    public function store(ClassCarRequest $request)
    {
        $data = ClassCar::create($request->all());

        return response()->json(['code' => 200, 'data' => $data], 200);
    }

    public function edit(ClassCar $item)
    {
        $url = route('dashboard.class.update', ['item' => $item]);
        $html = view('partials.form-class', ['url' => $url, 'type' => 'form_edit', 'item' => $item])->render();

        return response()->json(['data' => $html, 'code' => 200], 200);
    }

    public function update(ClassCarRequest $request, ClassCar $item)
    {
        $item->update($request->all());

        return response()->json(['data' => $item, 'code' => 200], 200);
    }

    public function destroy(ClassCar $item)
    {
        $item->delete();

        return response()->json(['code' => 200], 200);
    }
}
