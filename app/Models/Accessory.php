<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accessory extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'accessories';
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y',
        'deleted_at' => 'datetime',
    ];

    public function showPrice(String $currency = 'VND'): String
    {
        return number_format($this->price, 0, ',', ',') . ' ' . $currency;
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function use_objects()
    {
        return $this->hasMany(UseObject::class, 'accessory_id', 'id');
    }
}
