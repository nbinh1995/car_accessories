<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MakerCar extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'maker_cars';
    protected $guarded = [];
    protected $casts = [
        'created_at' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y',
        'deleted_at' => 'datetime',
    ];

    public function classCars()
    {
        return $this->hasMany(ClassCar::class, 'maker_car_id', 'id')->orderBy('updated_at', 'DESC');
    }
}
