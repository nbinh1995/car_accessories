<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModelCar extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'model_cars';
    protected $guarded = [];
    protected $casts = [
        'created_at' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y',
        'deleted_at' => 'datetime',
    ];

    public function classCar()
    {
        return $this->belongsTo(ClassCar::class, 'class_car_id', 'id');
    }

    public function objects()
    {
        return $this->hasMany(UseObject::class, 'model_car_id', 'id');
    }
}
