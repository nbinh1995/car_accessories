<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassCar extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'class_cars';
    protected $guarded = [];
    protected $casts = [
        'created_at' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y',
        'deleted_at' => 'datetime',
    ];

    public function modelCars()
    {
        return $this->hasMany(ModelCar::class, 'class_car_id', 'id')->orderBy('updated_at', 'DESC');
    }

    public function makerCar()
    {
        return $this->belongsTo(MakerCar::class, 'maker_car_id', 'id');
    }
}
