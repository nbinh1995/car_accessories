<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UseObject extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'use_objects';
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y',
        'deleted_at' => 'datetime',
    ];

    public function modelCar()
    {
        return $this->belongsTo(ModelCar::class, 'model_car_id', 'id');
    }

    public function accessory()
    {
        return $this->belongsTo(Accessory::class, 'accessory_id', 'id');
    }
}
