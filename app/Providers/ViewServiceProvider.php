<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['admin.accessory.create', 'admin.accessory.edit', 'admin.accessory.search'], 'App\Http\ViewComposers\CategoryComposer');
        view()->composer(['admin.accessory.create', 'admin.accessory.edit', 'admin.accessory.search', 'partials.form-class', 'partials.form-model'], 'App\Http\ViewComposers\CarComposer');
    }
}
